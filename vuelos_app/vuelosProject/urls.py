from django.contrib import admin
from django.urls import path
from rest_framework_simplejwt.views import (TokenObtainPairView, TokenRefreshView)
from vuelosApp import views



urlpatterns = [
    path('login/', TokenObtainPairView.as_view()),
    path('refresh/', TokenRefreshView.as_view()),
    path('user/', views.UserCreateView.as_view()),
    path('user/<int:pk>/', views.UserDetailView.as_view()),
    path('search/', views.BusquedaVueloView.as_view()), 
    path('fightsearch/', views.ReservaVueloBusquedaView.as_view()),
    path('crearReserva/', views.ReservaVueloCrearView.as_view()),
    path('addpassanger/', views.DetalleReservaCrearView.as_view()),
    path('detallesreserva/', views.ReservaVueloDetailView.as_view())
    ]