from django.contrib import admin
from .models.user import User
from .models.account import Account
from .models.ciudad import Ciudad
from .models.vuelos import Vuelos
from .models.reserva import Reserva
from .models.detalles_reserva import DetallesReserva


admin.site.register(User)
admin.site.register(Account)
admin.site.register(Ciudad)
admin.site.register(Vuelos)
admin.site.register(Reserva)
admin.site.register(DetallesReserva)