from django.db import models
from .vuelos import Vuelos
from .reserva import Reserva

class DetallesReserva (models.Model):
    id_det_reserva = models.AutoField (primary_key = True)
    id_reserva = models.ForeignKey (Reserva, related_name = "reserva", on_delete = models.CASCADE)
    id_vuelo = models.ForeignKey (Vuelos, related_name = "vuelo", on_delete = models.CASCADE)
    apellidoPasajero = models.CharField('Apellido', max_length = 50)
    nombrePasajero = models.CharField('Nombre ', max_length = 50)
    fechaNacimiento = models.DateField()
    genero = models.CharField('genero ', max_length = 50)
    tipoDocumento = models.CharField('Tipo Documento ', max_length = 50)
    documento = models.IntegerField()
    asiento = models.CharField('asiento', max_length = 6)