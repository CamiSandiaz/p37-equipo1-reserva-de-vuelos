from django.db import models
from .user import User

class Reserva (models.Model):
    id_reserva = models.CharField (primary_key=True, max_length=10)
    id_user = models.ForeignKey (User, related_name="usuario", on_delete=models.CASCADE)
    fecha_compra = models.DateField()
