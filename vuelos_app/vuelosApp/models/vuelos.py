from django.db import models
from .ciudad import Ciudad

class Vuelos(models.Model):
    id_vuelo = models.BigAutoField(primary_key=True)
    origen = models.ForeignKey(Ciudad, related_name='origen', on_delete=models.CASCADE)
    destino = models.ForeignKey(Ciudad, related_name='destino', on_delete=models.CASCADE)
    fecha = models.DateField()
    hora_salida = models.TimeField()
    duracion = models.IntegerField()
    disp_sillas = models.IntegerField()
    