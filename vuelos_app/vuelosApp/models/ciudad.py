from django.db import models

class Ciudad(models.Model):
    id_ciudad = models.AutoField(primary_key=True)
    nombre = models.CharField('nombre', max_length=50)
    aeropuerto = models.CharField('aeropuerto', max_length=150)
    estado =models.CharField('estado', max_length=50)