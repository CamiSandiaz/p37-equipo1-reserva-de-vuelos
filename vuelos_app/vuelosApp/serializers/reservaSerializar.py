from vuelosApp.models.reserva import Reserva
from vuelosApp.models.user import User
from rest_framework import serializers

class ReservaSerializer(serializers.ModelSerializer):
    class Meta:
        model = Reserva
        fields = ['id_reserva','id_user','fecha_compra']
        
    def create(self, validated_data):
        
        reservaInstance = Reserva.objects.create(**validated_data)
        return reservaInstance
    
    def to_representation(self, obj):
        reserva = Reserva.objects.get(id_reserva=obj.id_reserva)
        return {
            'idReserva': reserva.id_reserva,
            'id_user': reserva.id_user_id,
            'fecha': reserva.fecha_compra
        }
