from vuelosApp.models.detalles_reserva  import DetallesReserva
from vuelosApp.models.vuelos  import Vuelos
from vuelosApp.models.ciudad  import Ciudad
from vuelosApp.models.reserva    import Reserva

from rest_framework import serializers


class DetallesReservaSerializer(serializers.ModelSerializer):
    class Meta:
        model = DetallesReserva
        fields = ['id_reserva','apellidoPasajero','nombrePasajero','fechaNacimiento', 'genero', 'tipoDocumento','documento','asiento','id_vuelo']

    def to_representation(self, obj):
        detallesReserva = DetallesReserva.objects.get(id_det_reserva=obj.id_det_reserva)
        detallesVuelo = Vuelos.objects.get(id_vuelo=obj.id_vuelo_id)

        return{
                'IdReserva': detallesReserva.id_reserva_id,
                'nombrePasajero' : detallesReserva.nombrePasajero,
                'apellidoPasajero' : detallesReserva.apellidoPasajero,
                'tipoDocumento': detallesReserva.tipoDocumento,
                'documento' : detallesReserva.documento,
                'asiento': detallesReserva.asiento,
                'vuelo':{
                        'id_vuelo': detallesReserva.id_vuelo_id,
                        'origen': detallesVuelo.origen.nombre,
                        'destino': detallesVuelo.destino.nombre,
                        'fecha': detallesVuelo.fecha,
                        'horaSalida': detallesVuelo.hora_salida,
                        'duracion':detallesVuelo.duracion,
                }
                
        }