from vuelosApp.models.vuelos import Vuelos
from rest_framework import serializers

class VuelosSerializer(serializers.ModelSerializer):
    class Meta:
        model = Vuelos
        fields = ['id_vuelo','origen','destino','fecha', 'hora_salida', 'duracion','disp_sillas']

    def to_representation(self, obj):
        vuelos = Vuelos.objects.get(id_vuelo=obj.id_vuelo)
        return {
            'id_vuelo': vuelos.id_vuelo,
            'origen' : vuelos.origen.nombre,
            'destino' : vuelos.destino.nombre,
            'fecha' : vuelos.fecha, 
            'hora_salida': vuelos.hora_salida, 
            'duracion': vuelos.duracion,
            'disp_sillas': vuelos.disp_sillas
        }



