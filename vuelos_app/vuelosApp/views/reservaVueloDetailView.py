#Tipo GET. Este si necesita estar logueado
from django.conf import settings
from rest_framework import generics, status
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated
from rest_framework_simplejwt.backends import TokenBackend
from rest_framework import filters

from vuelosApp.models.detalles_reserva import DetallesReserva

from vuelosApp.serializers.detalles_reservaSerializar import DetallesReservaSerializer

class ReservaVueloDetailView(generics.ListAPIView):
    serializer_class = DetallesReservaSerializer
    filter_backends = [filters.SearchFilter]
    permission_classes = (IsAuthenticated,)

    def get_queryset(self):
        """
        token = self.request.META.get('HTTP_AUTHORIZATION')[7:]
        tokenBackend = TokenBackend(algorithm=settings.SIMPLE_JWT['ALGORITHM'])
        valid_data = tokenBackend.decode(token,verify=False)

        if valid_data['user_id'] != self.kwargs['user']:
            stringResponse = {'detail':'Unauthorized Request'}
            return Response(stringResponse, status=status.HTTP_401_UNAUTHORIZED)
        """
        queryset = DetallesReserva.objects.all()
        id_reserva = self.request.query_params.get('idreserva')
        lastName = self.request.query_params.get('apellido')

        if id_reserva is not None:
            queryset = queryset.filter(id_reserva__id_reserva=id_reserva)
        if lastName is not None:
            queryset = queryset.filter(apellidoPasajero=lastName)

        return queryset
