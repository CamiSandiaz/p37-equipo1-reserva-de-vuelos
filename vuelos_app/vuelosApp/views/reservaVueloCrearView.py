#Tipo POST. Este necesitar estar logeado, haber hecho una busqueda en reservaVueloBusquedaView 

from django.conf import settings
from django.http.response import ResponseHeaders
from rest_framework import generics, status
from rest_framework.response import Response
from rest_framework_simplejwt.backends import TokenBackend
from rest_framework.permissions import IsAuthenticated


from vuelosApp.models.reserva import Reserva
from vuelosApp.models.user import User
from vuelosApp.models.detalles_reserva import DetallesReserva
from vuelosApp.serializers.detalles_reservaSerializar import DetallesReservaSerializer
from vuelosApp.serializers.reservaSerializar import ReservaSerializer

class ReservaVueloCrearView(generics.CreateAPIView):
    queryset = Reserva.objects.all()
    serializer_class = ReservaSerializer
    permission_classes = (IsAuthenticated,)


    def post(self, request, *args, **kwargs):
        usernameDicc = request.data.pop('username')
        username = usernameDicc
        usuario = User.objects.get(username=username)
        request.data.update({'id_user':usuario.id})

        serializer = ReservaSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        
        return Response(serializer.data,status=status.HTTP_201_CREATED)