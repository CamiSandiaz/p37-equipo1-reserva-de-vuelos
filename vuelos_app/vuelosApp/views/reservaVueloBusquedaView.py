#Tipo GET y creo que POST. Este necesita estar logeado
from django.conf import settings
from rest_framework import generics
from rest_framework.response import Response
from rest_framework_simplejwt.backends import TokenBackend
from rest_framework.permissions import IsAuthenticated
from rest_framework import filters

from vuelosApp.models.vuelos import Vuelos

from vuelosApp.serializers.vuelosSerializer import VuelosSerializer

class ReservaVueloBusquedaView(generics.ListAPIView):
    
    serializer_class = VuelosSerializer
    filter_backends = [filters.SearchFilter]
    permission_classes = (IsAuthenticated,)

    def get_queryset(self):
        queryset = Vuelos.objects.all()
        """
        This view should return a list of all the purchases for
        the user as determined by the username portion of the URL.
        """
        origen = self.request.query_params.get('origen')
        destino = self.request.query_params.get('destino')
        fecha_salida = self.request.query_params.get('fecha')

        
        if origen is not None:
            queryset = queryset.filter(origen__nombre=origen)
        
        if destino is not None:
            queryset = queryset.filter(destino__nombre=destino)

        if fecha_salida is not None:
            queryset = queryset.filter(fecha=fecha_salida)
        
        return queryset

    #Necesitamos una funcion POST para obtener un id_vuelo y crear una reserva
