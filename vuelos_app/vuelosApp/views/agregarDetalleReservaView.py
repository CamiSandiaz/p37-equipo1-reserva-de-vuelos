from django.conf import settings
from django.http.response import ResponseHeaders
from rest_framework import generics, status
from rest_framework.response import Response
from rest_framework_simplejwt.backends import TokenBackend
from rest_framework.permissions import IsAuthenticated
from rest_framework import serializers


from vuelosApp.models.reserva import Reserva
from vuelosApp.models.detalles_reserva import DetallesReserva
from vuelosApp.serializers.detalles_reservaSerializar import DetallesReservaSerializer
from vuelosApp.serializers.reservaSerializar import ReservaSerializer

class DetalleReservaCrearView(generics.CreateAPIView):
    queryset = DetallesReserva.objects.all()
    serializer_class = DetallesReservaSerializer
    permission_classes = (IsAuthenticated,)

    def post(self, request, *args, **kwargs):
        serializer = DetallesReservaSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        
        return Response(serializer.data,status=status.HTTP_201_CREATED)