import { createRouter, createWebHashHistory } from 'vue-router';
import App from './App.vue';

import Search from './components/Search.vue';
import LogIn from './components/LogIn.vue';
import SignUp from './components/SignUp.vue';
import Home from './components/Home.vue';
import FlightSearch from './components/FlightSearch.vue';
import BookingSearch from './components/BookingSearch.vue';
import AddPassanger from './components/AddPassanger.vue';

const routes = [{
    path: '/',
    name: 'root',
    component: App
  },
  {
    path: '/user/search',
    name: "search",
    component: Search
  },
  {
    path: '/user/logIn',
    name: "logIn",
    component: LogIn
  },
  {
    path: '/user/signUp',
    name: "signUp",
    component: SignUp
  },
  {
    path: '/user/home',
    name: "home",
    component: Home
  },
  {
    path: '/user/flightSearch',
    name: "flightSearch",
    component: FlightSearch
  },
  {
    path: '/user/bookingSearch',
    name: "bookingSearch",
    component: BookingSearch
  },
  {
    path: '/user/addPassanger',
    name: "addPassanger",
    component: AddPassanger
  }
];

const router = createRouter({
  history: createWebHashHistory(),
  routes,
});

export default router;